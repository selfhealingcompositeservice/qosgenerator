package fr.dauphine.file;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.dauphine.service.QoSParameter;
import fr.dauphine.service.Service;

public class QWSReadFile {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	/**
	 * Returns the list of services and their qos from the QWS dataset
	 * @return the list of services with their their qos parameters
	 */
	public static List<Service> getServicesQoS() {
		
		List<Service> services = new ArrayList<Service>();
		
		Path path = Paths.get("qws.txt");
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	
	    	if(scanner.hasNextLine())
	    		scanner.nextLine();
	    	
	    	while (scanner.hasNextLine()){
	    		String [] line = scanner.nextLine().split(" ");
	    		//we are only interested by invocation using the direct method
	    		Service service = new Service(line[0]);
	    		
	    		QoSParameter responseTime = new QoSParameter();
	    		responseTime.setName("responseTime");
	    		responseTime.setValue(new Double(line[1]));
	    		//service.addQosParameter(responseTime);
	    		
	    		QoSParameter availability = new QoSParameter();
	    		availability.setName("availability");
	    		availability.setValue(new Double(line[3]));
	    		//service.addQosParameter(availability);
	    		
	    		services.add(service);
	    	}      

	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return services;
	}
	
}
