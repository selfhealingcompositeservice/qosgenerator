package fr.dauphine.file;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import fr.dauphine.service.QoSParameter;
import fr.dauphine.service.Service;

public class FileUtils {
	
	/**
	 * Writes the services with their QoS to a file
	 * @param  filename  the file write the services
	 */
	public static void writeServices(String filename, List<Service> services) {
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(filename, "UTF-8");
			
			for(Service service:services) {
				writer.print(service.getName());
				/*for(QoSParameter qos:service.getQosParameters()) {
					writer.print(" ");
					writer.print(qos.getValue());
				}*/
				writer.println();
			}
			
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
