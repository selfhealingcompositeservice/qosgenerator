package fr.dauphine.file;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import fr.dauphine.service.QoSParameter;
import fr.dauphine.service.Service;

public class KomReadFile {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	final static String ACCESS_METHOD = "'direct'";
	final static String ERROR_MSG = "'OK'";
	private static Map<String, Integer> serviceOcurrences = new HashMap<String, Integer>();
	private static Map<String, Double> serviceResponseTime = new HashMap<String, Double>();
	private static Map<String, Integer> serviceFailuresTimes = new HashMap<String, Integer>();

	
	
	/**
	 * Returns the list of services and their qos from the KOM dataset
	 * @return the list of services with their their qos parameters
	 */
	public static List<Service> getServicesQoS() {
		
		List<Service> services = new ArrayList<Service>();
		
		Path path = Paths.get("sequential.txt");
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String [] line = scanner.nextLine().split(",");
	    		//we are only interested by invocation using the direct method
	    		if(line[4].trim().equals(ACCESS_METHOD)) {
	    			addService(line);
	    		}
	    	}      
	    	
	    	services = calculateAverages();
	    	
	    } catch (IOException e) {
			e.printStackTrace();
		}
		
		return services;
	}
	
	@SuppressWarnings("unused")
	private static void addService(String [] line) {
		
		String name = line[3].trim();
		String timetoserver  = line[5].trim();
		String timetoclient  = line[6].trim();
		String processtime  = line[7].trim();
		String responsetime = line[8].trim();
		String errormsg  = line[9].trim();
		String available = line[10].trim().substring(0, 1);
		
		if(serviceOcurrences.containsKey(name)) {
			serviceOcurrences.put(name, serviceOcurrences.get(name) + 1);
			
			if(errormsg.equals(ERROR_MSG)) {
				//completed successfully

				serviceResponseTime.put(name, serviceResponseTime.get(name) + new Double(responsetime));
			} else {
				serviceFailuresTimes.put(name, serviceFailuresTimes.get(name) + 1);
			}
		} else {
			serviceOcurrences.put(name, 1);
			
			if(errormsg.equals(ERROR_MSG)) {
				//completed successfully
				serviceResponseTime.put(name, new Double(responsetime));
				serviceFailuresTimes.put(name, 0);
			} else {
				serviceResponseTime.put(name, 0.0);
				serviceFailuresTimes.put(name, 1);
			}
		}
		//System.out.println(serviceOcurrences);
	}
	

	private static List<Service> calculateAverages() {
		
		List<Service> services = new ArrayList<Service>();
		
		for(String name:serviceOcurrences.keySet()) {
			
			Service service = new Service(name.replaceAll("'", ""));
			
			QoSParameter responseTime = new QoSParameter();
			responseTime.setName("responseTime");
			
			double d = serviceOcurrences.get(name)-serviceFailuresTimes.get(name);
			d = serviceResponseTime.get(name) / d;
			responseTime.setValue(d);
			//service.addQosParameter(responseTime);
			
			QoSParameter availability = new QoSParameter();
			availability.setName("availability");
			
			d = serviceOcurrences.get(name)-serviceFailuresTimes.get(name);
			d /= serviceOcurrences.get(name);
			availability.setValue(d);
			//service.addQosParameter(availability);
			
			
			
			services.add(service);
		}
		
		return services;
	}

}
