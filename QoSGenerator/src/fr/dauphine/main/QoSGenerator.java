package fr.dauphine.main;

import java.util.List;

import fr.dauphine.file.FileUtils;
import fr.dauphine.file.KomReadFile;
import fr.dauphine.file.QWSReadFile;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.qos.QoSMapper;
import fr.dauphine.service.Service;

public class QoSGenerator {
	
	private final static String DATASET_KOM = "kom";
	private final static String DATASET_QWS = "qws";
	
	private void run(String fileGraph, String fileQoS, String dataset) {
		
		List<String> services = GraphReader.getServices(fileGraph);
		List<Service> servicesQos;
		
		switch (dataset) {
        case DATASET_KOM:
        	servicesQos = KomReadFile.getServicesQoS();
            break;
                
        case DATASET_QWS:
        	servicesQos = QWSReadFile.getServicesQoS();
            break;
                    
        default:
        	servicesQos = QWSReadFile.getServicesQoS();
            break;
		}
				
		List<Service> mappedServices = QoSMapper.mapQoStoServices(services,servicesQos);
		FileUtils.writeServices(fileQoS, mappedServices);
	}

	public static void main(String[] args) {
		
		(new QoSGenerator()).run(args[0], args[1], args[2]);
	}

}
