package fr.dauphine.qos;

import java.util.ArrayList;
import java.util.List;

import fr.dauphine.service.Service;

public class QoSMapper {
	
	public static List<Service> mapQoStoServices(List<String> services, List<Service> qosParameters) {
		List<Service> servicesQos = new ArrayList<Service>();
		
		int qosNumber = qosParameters.size();
		int i = 0;
		
		for(String name:services) {
			
			if(i == qosNumber)
				i = 0;
			
			Service service = new Service(name);
			//service.setQosParameters(qosParameters.get(i++).getQosParameters());
			servicesQos.add(service);
		}
		
		return servicesQos;
	}

}
