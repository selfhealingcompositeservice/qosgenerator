package fr.dauphine.dataset;

public class WSDream {

	public static final double TIME_MEAN = 1430; //milliseconds
	public static final double TIME_STD = 31900;
	
	public static final double FAILURE_PROBABILITY_MEAN = 4.05; //percentage 0 means no failure
	public static final double FAILURE_PROBABILITY_STD = 17.32;
	
	//The price was not included in the WSDream QoS dataset
	public static final double PRICE_MEAN = 20; 
	public static final double PRICE_PROBABILITY_STD = 10;
}
